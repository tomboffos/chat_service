package message

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/tomboffos/chat_service/pkg/common/models"
	"net/http"
)

func (h handler) DeleteMessage(c *gin.Context) {
	id := c.Param("id")

	if res := h.DB.Delete(&models.Message{}, id); res.Error != nil {
		c.AbortWithError(http.StatusNotFound, res.Error)
		return
	}

	c.JSON(http.StatusOK, gin.H{})
	return
}
