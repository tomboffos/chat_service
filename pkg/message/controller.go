package message

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/tomboffos/chat_service/pkg/common/socket"
	"gorm.io/gorm"
)

type handler struct {
	DB             *gorm.DB
	SocketInstance socket.Socket
}

func RegisterRoutes(r *gin.Engine, db *gorm.DB, socket socket.Socket) {
	h := handler{
		DB:             db,
		SocketInstance: socket,
	}

	routes := r.Group("messages")
	{
		routes.POST("/", h.SendMessage)
		routes.DELETE("/:id", h.DeleteMessage)
		routes.PUT("/:id", h.UpdateMessage)
	}
}
