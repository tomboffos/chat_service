package message

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/tomboffos/chat_service/pkg/common/models"
	"net/http"
)

type UpdateMessageBody struct {
	Text string `json:"text"`
}

func (h handler) UpdateMessage(c *gin.Context) {
	id := c.Param("id")

	body := UpdateMessageBody{}

	if err := c.BindJSON(&body); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	var message models.Message

	if res := h.DB.First(&message, id); res.Error != nil {
		c.AbortWithError(http.StatusNotFound, res.Error)
		return
	}

	message.Text = body.Text

	if res := h.DB.Save(&message); res.Error != nil {
		c.AbortWithError(http.StatusInternalServerError, res.Error)
		return
	}

	c.JSON(http.StatusOK, gin.H{})
	return
}
