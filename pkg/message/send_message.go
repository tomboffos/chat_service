package message

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/tomboffos/chat_service/pkg/common/models"
	"net/http"
)

type SendMessageBody struct {
	UserId      uint   `json:"user_id"`
	ChatId      uint   `json:"chat_id"`
	Text        string `json:"text"`
	RepliedToId *uint  `json:"replied_to_id"`
}

func (h handler) SendMessage(c *gin.Context) {
	body := SendMessageBody{}

	if err := c.BindJSON(&body); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	var message models.Message

	message.ChatId = body.ChatId
	message.UserId = body.UserId
	message.Text = body.Text
	message.RepliedToId = body.RepliedToId

	var chatParticipants []models.ChatParticipant

	if res := h.DB.Where("chat_id = ?", message.ChatId).Find(&chatParticipants); res.Error != nil {
		c.AbortWithError(http.StatusNotFound, res.Error)
		return
	}

	for _, participants := range chatParticipants {

		if h.SocketInstance.Manager.Clients[int(participants.UserId)] != nil {
			err := h.SocketInstance.Manager.Clients[int(participants.UserId)].SOCKET.WriteJSON(message)

			if err != nil {
				c.AbortWithError(http.StatusInternalServerError, err)
				return
			}
		}
	}

	if res := h.DB.Create(&message); res.Error != nil {
		c.AbortWithError(http.StatusInternalServerError, res.Error)
		return
	}

	c.JSON(http.StatusCreated, message)
	return
}
