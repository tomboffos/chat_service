package chat

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/tomboffos/chat_service/pkg/common/socket"
	"gorm.io/gorm"
)

type handler struct {
	DB             *gorm.DB
	SocketInstance socket.Socket
}

func RegisterRoutes(r *gin.Engine, db *gorm.DB, socket socket.Socket) {
	h := handler{
		DB:             db,
		SocketInstance: socket,
	}

	routes := r.Group("chats")
	{
		routes.GET("/", h.GetChats)
		routes.GET("/ws/:id", h.ConnectChat)
		routes.GET("/by-place/:id", h.GetByPlace)
		routes.POST("/update/:id", h.UpdateChat)
		routes.GET("/join/:id", h.JoinToChat)
		routes.GET("/get-info/:id", h.GetChat)
	}
}
