package chat

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/tomboffos/chat_service/pkg/common/models"
	"net/http"
)

func (h handler) GetChats(c *gin.Context) {
	userId := c.Query("user_id")

	var chats []models.Chat
	var participants []models.ChatParticipant

	h.DB.Table("chat_participants").Where("user_id = ?", userId).Find(&participants)

	var participantIds []uint

	for _, participant := range participants {
		participantIds = append(participantIds, participant.ChatId)
	}

	h.DB.Find(&chats, participantIds)

	c.JSON(http.StatusOK, chats)
	return
}
