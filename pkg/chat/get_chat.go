package chat

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/tomboffos/chat_service/pkg/common/models"
	"net/http"
)

func (h handler) GetChat(c *gin.Context) {
	id := c.Param("id")

	var chat models.Chat

	if err := h.DB.Model(&models.Chat{}).Preload("Messages").First(&chat, id).Error; err != nil {
		c.AbortWithError(http.StatusNotFound, err)
		return
	}

	c.JSON(http.StatusOK, chat)
	return
}
