package chat

import (
	"errors"
	"github.com/gin-gonic/gin"
	"gitlab.com/tomboffos/chat_service/pkg/common/models"
	"gitlab.com/tomboffos/chat_service/pkg/gateways"
	"gorm.io/gorm"
	"net/http"
	"strconv"
)

func (h handler) GetByPlace(c *gin.Context) {
	place := c.Param("id")

	var chat models.Chat

	if result := h.DB.Table("chats").
		Where("chats.place_id = ?", place).
		Select("chats.*, COUNT(chat_participants.id) as children_count").
		Joins("LEFT JOIN chat_participants ON chats.id = chat_participants.chat_id").
		Group("chats.id").
		Order("children_count DESC").
		First(&chat); result.Error != nil {
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {

			placeResponse := gateways.GetPlaceById(place)

			var chatToCreation models.Chat

			placeId, _ := strconv.ParseUint(place, 10, 0)

			chatToCreation.PlaceId = uint(placeId)
			chatToCreation.Title = placeResponse.Title

			h.DB.Create(&chatToCreation)

			c.JSON(http.StatusOK, chatToCreation)
			return
		} else {

			c.AbortWithError(http.StatusBadRequest, result.Error)
			return
		}

	}

	c.JSON(http.StatusOK, chat)
	return
}
