package chat

import (
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"gitlab.com/tomboffos/chat_service/pkg/common/models"
	"log"
	"net/http"
	"strconv"
)

func (h handler) ConnectChat(c *gin.Context) {

	conn, err := h.SocketInstance.Upgrader.Upgrade(c.Writer, c.Request, nil)

	id := c.Param("id")
	intId, err := strconv.Atoi(id)

	client := &models.Client{ID: intId, SOCKET: conn}

	if err != nil {
		log.Printf("%s, error while Upgrading websocket connection\n", err.Error())
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	conn.WriteJSON(gin.H{
		"message": "Connected",
	})

	h.SocketInstance.Manager.Clients[intId] = client

	for {
		_, _, err := conn.ReadMessage()
		if err != nil {
			if websocket.IsCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				// Handle disconnect event
				return
			}
			delete(h.SocketInstance.Manager.Clients, intId)
			return
		}

	}
}
