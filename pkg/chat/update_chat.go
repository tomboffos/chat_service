package chat

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/tomboffos/chat_service/pkg/common/models"
	"gitlab.com/tomboffos/chat_service/pkg/gateways"
	"net/http"
)

type ChatUpdateBody struct {
	Title       string `json:"title"`
	Description string `json:"description"`
}

func (h handler) UpdateChat(c *gin.Context) {
	body := ChatUpdateBody{}

	body.Title = c.Request.FormValue("title")
	body.Description = c.Request.FormValue("description")

	chatId := c.Param("id")

	c.Request.ParseMultipartForm(10 << 20)

	if err := c.ShouldBind(&body); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	var chat models.Chat

	if result := h.DB.First(&chat, chatId); result.Error != nil {
		c.AbortWithError(http.StatusNotFound, result.Error)
		return
	}

	chat.Title = body.Title
	chat.Description = body.Description

	file, _ := c.FormFile("file")

	if file != nil {
		urlMedia, _ := gateways.SaveMedia(file)

		chat.Avatar = *urlMedia
	}

	h.DB.Save(&chat)

	c.JSON(http.StatusOK, &chat)
	return
}
