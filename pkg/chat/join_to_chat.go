package chat

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/tomboffos/chat_service/pkg/common/models"
	"net/http"
)

type JoinToChatBody struct {
	UserId uint `json:"user_id"`
	ChatId uint `json:"chat_id"`
}

func (h handler) JoinToChat(c *gin.Context) {
	body := JoinToChatBody{}

	if err := c.BindJSON(&body); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}
	var chatParticipant models.ChatParticipant

	chatParticipant.ChatId = body.ChatId
	chatParticipant.UserId = body.UserId

	if result := h.DB.Create(&chatParticipant); result.Error != nil {
		c.AbortWithError(http.StatusInternalServerError, result.Error)
		return
	}

	var participants []models.ChatParticipant

	if result := h.DB.Where("chat_id = ?", chatParticipant.ChatId).Find(&participants); result.Error != nil {
		c.AbortWithError(http.StatusInternalServerError, result.Error)
		return
	}

	for _, participant := range participants {
		h.SocketInstance.Manager.Clients[int(participant.UserId)].SOCKET.WriteJSON(&models.Message{
			Text:   "New participant joined",
			ChatId: chatParticipant.ChatId,
		})
	}

	c.JSON(http.StatusOK, gin.H{})
	return

}
