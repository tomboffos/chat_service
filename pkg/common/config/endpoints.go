package config

type Endpoints struct {
	PlacesApi string `mapstructure:"PLACES_API"`
	ChatsApi  string `mapstructure:"CHATS_API"`
	BaseApi   string `mapstructure:"BASE_API"`
}

func GetEndpoints() Endpoints {
	return Endpoints{
		"http://127.0.0.1:3002",
		"http://127.0.0.1:3001",
		"http://127.0.0.1:3000",
	}
}
