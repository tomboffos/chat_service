package db

import (
	"gitlab.com/tomboffos/chat_service/pkg/common/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"log"
)

func Init(url string) *gorm.DB {
	db, err := gorm.Open(postgres.Open(url), &gorm.Config{})

	if err != nil {
		log.Fatalln(err)
	}

	db.AutoMigrate(&models.Chat{}, &models.ChatParticipant{}, &models.Message{})

	return db
}
