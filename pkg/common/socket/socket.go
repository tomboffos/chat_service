package socket

import (
	"github.com/gorilla/websocket"
	"gitlab.com/tomboffos/chat_service/pkg/common/models"
)

type Socket struct {
	Upgrader websocket.Upgrader
	Manager  models.ClientManager
}
