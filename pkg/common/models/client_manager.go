package models

type ClientManager struct {
	Clients    map[int]*Client
	Broadcast  chan []byte
	Register   chan *Client
	Unregister chan *Client
}
