package models

import "github.com/gorilla/websocket"

type Client struct {
	ID     int
	SOCKET *websocket.Conn
	SEND   chan []byte
}
