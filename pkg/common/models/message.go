package models

import (
	"github.com/lib/pq"
	"gorm.io/gorm"
)

type Message struct {
	gorm.Model
	UserId      uint           `json:"user_id"`
	ChatId      uint           `json:"chat_id"`
	Text        string         `json:"text"`
	Medias      pq.StringArray `json:"medias" gorm:"type:text[]"`
	RepliedToId *uint          `json:"replied_to_id"`
	RepliedTo   *Message       `gorm:"references:RepliedToId"`
}
