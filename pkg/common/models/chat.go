package models

import "gorm.io/gorm"

type Chat struct {
	gorm.Model
	PlaceId          uint              `json:"place_id"`
	Avatar           string            `json:"avatar"`
	Title            string            `json:"title"`
	Description      string            `json:"description"`
	ChatParticipants []ChatParticipant `json:"participants"`
	Messages         []Message         `json:"messages"`
}
