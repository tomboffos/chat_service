package models

import "gorm.io/gorm"

type ChatParticipant struct {
	gorm.Model
	UserId uint   `json:"user_id"`
	ChatId uint   `json:"chat_id"`
	Prefix string `json:"prefix"`
}
