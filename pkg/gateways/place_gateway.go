package gateways

import (
	"encoding/json"
	"gitlab.com/tomboffos/chat_service/pkg/common/config"
	"io/ioutil"
	"net/http"
)

type PlaceByIdResponse struct {
	Title   string `json:"title"`
	UserId  uint   `json:"user_id"`
	Address string `json:"address"`
}

func GetPlaceById(id string) *PlaceByIdResponse {
	client := http.Client{}

	placeEndpoint := config.GetEndpoints().PlacesApi

	resp, err := client.Get(placeEndpoint + "/places/by-id/" + id)

	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()

	place := &PlaceByIdResponse{}

	responseBody, err := ioutil.ReadAll(resp.Body)

	json.Unmarshal(responseBody, &place)

	return place
}
