package gateways

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.com/tomboffos/chat_service/pkg/common/config"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"strconv"
)

type MediaResponse struct {
	Media MediaBody `json:"media"`
}

type MediaBody struct {
	ID uint `json:"ID"`
}

func SaveMedia(fileHeader *multipart.FileHeader) (*string, error) {
	baseApi := config.GetEndpoints().BaseApi

	url := baseApi + "/media"

	// Create a new buffer to write the request body
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)

	// Add fields to the request

	// Create a part for the file
	part, err := writer.CreateFormFile("file", fileHeader.Filename)
	if err != nil {
		fmt.Println("Error creating form file:", err)
		return nil, err
	}

	file, err := fileHeader.Open()

	defer file.Close()

	_, err = io.Copy(part, file)

	if err != nil {
		fmt.Println("Error copying file content:", err)
		return nil, err
	}

	// Close the multipart writer to finalize the request body
	writer.Close()

	// Create the HTTP request
	req, err := http.NewRequest("POST", url, body)
	if err != nil {

		return nil, err
	}
	req.Header.Set("Content-Type", writer.FormDataContentType())

	// Perform the request
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {

		return nil, err
	}
	defer resp.Body.Close()

	responseMediaBody := &MediaResponse{}

	responseBody, err := ioutil.ReadAll(resp.Body)

	json.Unmarshal(responseBody, &responseMediaBody)

	mediaId := strconv.Itoa(int(responseMediaBody.Media.ID))
	mediaUrl := url + "/" + mediaId
	return &mediaUrl, nil
}
