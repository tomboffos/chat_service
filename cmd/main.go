package main

import (
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"github.com/spf13/viper"
	"gitlab.com/tomboffos/chat_service/pkg/chat"
	"gitlab.com/tomboffos/chat_service/pkg/common/db"
	"gitlab.com/tomboffos/chat_service/pkg/common/models"
	"gitlab.com/tomboffos/chat_service/pkg/common/socket"
	"gitlab.com/tomboffos/chat_service/pkg/message"
	"gorm.io/gorm"
	"net/http"
)

func setupRouter(h *gorm.DB) *gin.Engine {
	r := gin.Default()

	socketInstance := socket.Socket{
		Upgrader: websocket.Upgrader{
			ReadBufferSize:  1024 * 1024 * 1024,
			WriteBufferSize: 1024 * 1024 * 1024,
			//Solving cross-domain problems
			CheckOrigin: func(r *http.Request) bool {
				return true
			},
		},
		Manager: models.ClientManager{
			Broadcast:  make(chan []byte),
			Register:   make(chan *models.Client),
			Unregister: make(chan *models.Client),
			Clients:    make(map[int]*models.Client),
		},
	}

	chat.RegisterRoutes(r, h, socketInstance)
	message.RegisterRoutes(r, h, socketInstance)

	return r
}

func main() {
	viper.SetConfigFile("./pkg/common/envs/.env")
	viper.ReadInConfig()

	port := viper.Get("PORT").(string)
	dbUrl := viper.Get("DB_URL").(string)

	h := db.Init(dbUrl)
	r := setupRouter(h)

	r.Run(port)
}
